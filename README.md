# Meta2ArcMenu
It seems very difficult to activate the ArcMenu whith the meta/super key. This program attempts to solve this by intercepting the meta key.

## How this plugin works
This plugin works by watching for meta events, then it will trigger a down event, so that window interactions are possible(window snapping).
If the meta key is pressed for no longer than 500ms then it will trigger another key, F7, that can be used to activate ArcMenu.
Notice that you should bind the hotkey in ArcMenu to Super+F7, because the meta key is released after sending the F7 key event.

## Dependencies

- [Interception Tools](https://gitlab.com/interception/linux/tools)

## Building

```
$ git clone git@gitlab.com:MagneFire/meta2arcmenu.git
$ cd meta2arcmenu
$ mkdir build
$ cd build
$ cmake ..
$ make
```

## Execution

`meta2arcmenu` is an [Interception Tools](https://gitlab.com/interception/linux/tools) plugin. A suggested
`udevmon` job configuration is:

```yaml
- JOB: "intercept -g $DEVNODE | meta2arcmenu | uinput -d $DEVNODE"
  DEVICE:
    EVENTS:
      EV_KEY: [KEY_LEFTMETA]

```

For more information about the [Interception Tools](https://gitlab.com/interception/linux/tools), check
the project's website.

## Sources
- Original code: https://gitlab.com/interception/linux/plugins/caps2esc
- Timeout code: https://gitlab.com/mar04/caps2esc