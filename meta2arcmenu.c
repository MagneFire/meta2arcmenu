#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <linux/input.h>

#include <time.h>

// Set timeout for 500ms.
#define LONG_PRESS 500

// clang-format off
const struct input_event
meta_up         = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 0},
meta_down       = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 1},
meta_repeat     = {.type = EV_KEY, .code = KEY_LEFTMETA, .value = 2},
trigger_up      = {.type = EV_KEY, .code = KEY_F7,       .value = 0},
trigger_down    = {.type = EV_KEY, .code = KEY_F7,       .value = 1},
trigger_repeat  = {.type = EV_KEY, .code = KEY_F7,       .value = 2},
ctrl_up         = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 0},
ctrl_down       = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 1},
ctrl_repeat     = {.type = EV_KEY, .code = KEY_LEFTCTRL, .value = 2},
syn             = {.type = EV_SYN, .code = SYN_REPORT,   .value = 0};
// clang-format on

int equal(const struct input_event *first, const struct input_event *second) {
    return first->type == second->type && first->code == second->code &&
           first->value == second->value;
}

int read_event(struct input_event *event) {
    return fread(event, sizeof(struct input_event), 1, stdin) == 1;
}

void write_event(const struct input_event *event) {
    if (fwrite(event, sizeof(struct input_event), 1, stdout) != 1)
        exit(EXIT_FAILURE);
}

int main(void) {
    int super_is_down = 0, trigger_give_up = 0;
    struct input_event input;
    struct timespec start, end;
    unsigned long long delta_ms;

    setbuf(stdin, NULL), setbuf(stdout, NULL);

    while (read_event(&input)) {
        if (input.type == EV_MSC && input.code == MSC_SCAN) {
            continue;
        }

        if (input.type != EV_KEY) {
            write_event(&input);
            continue;
        }

        // The repeat trigger has an undefined interval, so use CLOCK_MONOTONIC_RAW to keep track of time.
        if (equal(&input, &meta_repeat)) {
            clock_gettime(CLOCK_MONOTONIC_RAW, &end);
            delta_ms = (end.tv_sec - start.tv_sec) * 1000 + (end.tv_nsec - start.tv_nsec) / 1000000;
            if (delta_ms > LONG_PRESS) {
                trigger_give_up = 1;
            } 
        }

        if (super_is_down) {
            if (equal(&input, &meta_down) ||
                equal(&input, &meta_repeat))
                continue;

            if (equal(&input, &meta_up)) {
                super_is_down = 0;
                // The meta key is held down for to long, or another key was pressed, assume window manipulation.
                if (trigger_give_up) {
                    trigger_give_up = 0;

                    // 'Press' control to cancel meta event.
                    // This prevents the overview trigger when the meta key is hold longer than LONG_PRESS.
                    write_event(&ctrl_down);
                    write_event(&syn);
                    usleep(20000);
                    write_event(&ctrl_up);
                    write_event(&meta_up);
                    continue;
                }
                // Trigger key event for ArcMenu. Cancel meta event.
                write_event(&trigger_down);
                write_event(&syn);
                usleep(20000);
                write_event(&trigger_up);
                write_event(&meta_up);
                continue;
            }

            // A different key was pressed whle holding the meta key, cancel ArcMenu trigger.
            if (!trigger_give_up) {
                trigger_give_up = 1;
            }
        } else if (equal(&input, &meta_down)) {
            // Keep track of how long the meta key is held down.
            clock_gettime(CLOCK_MONOTONIC_RAW, &start);
            super_is_down = 1;
            // Also write the meta_down event below.
        }

        write_event(&input);
    }
}
